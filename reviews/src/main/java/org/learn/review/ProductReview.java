package org.learn.review;

import com.fasterxml.jackson.annotation.JsonProperty;

record ProductReview(
        @JsonProperty("ptn")
        String ptn,

        @JsonProperty("rating")
        Double rating,

        @JsonProperty("reviews")
        String[] reviews) {
}
