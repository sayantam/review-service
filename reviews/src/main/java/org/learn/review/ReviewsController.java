package org.learn.review;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;
import io.micronaut.http.client.HttpClient;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.scheduling.TaskExecutors;
import io.micronaut.scheduling.annotation.ExecuteOn;
import jakarta.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import reactor.core.publisher.Mono;

@Controller("/{ptn}/reviews")
public class ReviewsController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Client("http://ratings-service:8080")
    @Inject
    HttpClient httpClient;

    @Get(produces = MediaType.APPLICATION_JSON)
    @ExecuteOn(TaskExecutors.IO)
    public Mono<ProductReview> index(@PathVariable String ptn) {
        logger.debug("Processing request for ptn: {}", ptn);
        var response = Mono.
                from(httpClient.retrieve(HttpRequest.GET("/" + ptn + "/ratings/average"), ProductRating.class));
        var reviews = new String[] { "just ok", "da best", "satisfied" };
        return response.map(productRating -> new ProductReview(ptn, productRating.averageRating(), reviews));
    }
}
