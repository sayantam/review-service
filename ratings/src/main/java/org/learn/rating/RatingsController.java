package org.learn.rating;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.PathVariable;

import java.util.Map;

@Controller("/{ptn}/ratings")
public class RatingsController {

    @Get(uri = "/average", produces = MediaType.APPLICATION_JSON)
    public Map<String, Object> fetchAverage(@PathVariable String ptn) {
        return Map.of("ptn", ptn, "averageRating", 4.5);
    }
}
