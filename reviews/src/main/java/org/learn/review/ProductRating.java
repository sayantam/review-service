package org.learn.review;

import com.fasterxml.jackson.annotation.JsonProperty;

record ProductRating(
        @JsonProperty("ptn")
        String ptn,

        @JsonProperty("averageRating")
        Double averageRating) {
}
